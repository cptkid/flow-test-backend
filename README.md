# flow-test-backend

## Decisiones de diseño
*  Para el desarrollo y testeo del ejercicio, se utilizó NodeJS v8.12.0, tanto para comunicarse con la API externa, así como para crear la API intermediaria que disponibiliza los servicios para nuestra aplicación cliente (https://gitlab.com/cptkid/flow-test-frontend) .
*  Se utilizó el Framework Fastify para administrar el enrutamiento, puesto que es el Framework que se utiliza actualmente en la compañía.
*  Para realizar las pruebas unitarias, se utilizó Tap, junto con SuperTest.
*  La API externa utilizada para obtener los datos del tiempo es Open Weather Map (https://openweathermap.org/).
*  La API externa utilizada para obtener la IP del cliente es Ipify (https://www.ipify.org/).
*  La API externa utilizada para obtener la geolocalización del cliente a partir de su IP es ip-api (http://ip-api.com).

## Análisis preliminar
En una primera aproximación al problema, se revisó la documentación de las tecnologías y APIs a utilizar, para entender el alcance de las mismas y la usabilidad aplicable al caso de estudio en cuestión.

## Desarrollo
Los pasos seguidos durante el desarrollo fueron: 
*  Programación de los servicios para consumir las APIs externas.
*  Programación de los servicios que expone el servidor de cara al cliente.
*  Parametrización y encapsulamiento de los métodos, reutilización de código.
*  Escritura de los tests unitarios.

## Potenciales mejoras
- Búsqueda de pronósticos para múltiples ciudades.
- Incluir comentarios dentro del código fuente para documentar.

# Instalación

## Utilizando Docker (Requiere tener Docker instalado)
Copiar el siguiente script (testeado en UBUNTU 18.04.2) que:
- Clona localmente el repositorio,
- Levanta un contenedor en Docker,
- Ejecuta la aplicación,
- Expone el puerto 


```
git clone https://gitlab.com/cptkid/flow-test-backend.git
cd flow-test-backend
npm install
docker build -t flowtest .
docker run -p 3000:3000 flowtest
```
Acceder a la aplicación en `http://localhost:3000`


## Utilizando la cli de NodeJS
Copiar el siguiente script (testeado en UBUNTU 18.04.2) que:
- Clona localmente el repositorio,
- Ejecuta la aplicación,
- Expone el puerto 3000

```
git clone https://gitlab.com/cptkid/flow-test-backend.git
cd flow-test-backend
npm install
PORT=3000 node app.js
```
Acceder a la aplicación en `http://localhost:3000`

## Unit Test
Para ejecutar los tests unitarios sobre la API, copiar el siguiente script (testeado en UBUNTU 18.04.2) que:
- Clona localmente el repositorio,
- Ejecuta Node TAP para lanzar los tests,
- Imprime en la consola el resultado de los mismos.
```
git clone https://gitlab.com/cptkid/flow-test-backend.git
cd flow-test-backend
npm install
npm test
```


## Consumo de la API
Los servicios expuestos por la API son:
*  /v1/location/ Devuelve la ubicación actual del cliente en base a su IP
*  /v1/current/ Devuelve el pronóstico del tiempo actual para la posición actual del cliente en base a su IP
*  /v1/current/:city Devuelve el pronóstico del tiempo actual para la ciudad deseada (:city)
*  /v1/forecast/ Devuelve el pronóstico del tiempo extendido a 5 días para la posición actual del cliente en base a su IP
*  /v1/forecast/:city Devuelve el pronóstico del tiempo extendido a 5 días para la ciudad deseada (:city)

Ejemplo de usabilidad:
```
http://localhost:3000/v1/location
```

###### Consultas, dudas o problemas, comunicarse a faupablo@gmail.com o contacto@pablo.fau.com.ar