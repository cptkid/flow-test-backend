const Fastify = require('fastify');

server = () => {
    const fastify = Fastify();
   
    //Para ambiente de Test, se habilitan todos los origenes, a modo de facilitar el consumo de la API
    fastify.register(require('fastify-cors'), { 
        allow: '*'
    })
    
    fastify.register(require('./routes/mainline'))

    fastify.listen(process.env.PORT||3000, function (err, address) {
        if (err) {
        fastify.log.error(err)
        process.exit(1)
        }
        fastify.log.info(`Servidor Online en ${address}`)
    })
    return fastify
}

server();

module.exports = server;



  