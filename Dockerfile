FROM node:8

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install


COPY . .

EXPOSE 3000
CMD PORT=3000 node app.js