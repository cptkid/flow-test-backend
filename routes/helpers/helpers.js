const axios = require('axios');
const env = require('../../config/development');
const openWeatherURL = env.openWeatherURL;
const openWeatherAPIKey = env.weatherapikey;

const helpers = {
     publicIP : async () => {
        const response = await axios.get('https://api.ipify.org?format=json');
        return response.data.ip
    },
     geoIP : async (ip) => {
        const response = await axios.get('http://ip-api.com/json/'+ip);
        return response.data
    },
     currentWeather : async (city,countrycode=null) => {
        const query  = (countrycode? city + ',' + countrycode : city ) + '&appid=' + openWeatherAPIKey;;
        let url = openWeatherURL + 'weather' + '?q=' + query 
        try {
            const response = await axios.get(url)
            return response.data;
        }catch(error){
            return {"status":404}
        }
        

    },
     forecastWeather : async (city,countrycode=null) => {
        const query  = (countrycode? city + ',' + countrycode : city ) + '&appid=' + openWeatherAPIKey;;
        let url = openWeatherURL + 'forecast' + '?q=' + query 
        try {
            const response = await axios.get(url)
            return response.data;
        }catch(error){
            return {"status":404}
        }
    }
}

module.exports = helpers