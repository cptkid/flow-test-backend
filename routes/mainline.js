const helpers = require("./helpers/helpers");
let node_environment = process.env.ENV || 'development'
const env = require('../config/' + node_environment);

const routes = async (fastify, options) => {

    fastify.get('/', async (req, res) => {      
         return  await ( req.ip == '127.0.0.1' )? helpers.publicIP() : req.ip;
    })
  
    fastify.get(env.apiversion + '/location', async (req, res) => {
        const ip = await helpers.publicIP();
        const geoInfo = await helpers.geoIP(ip);

        return geoInfo;
    })

    fastify.get(env.apiversion + '/current/:city', async (req, res) => {
        if(req.params.city === ''){
            let ip = await helpers.publicIP();
            let geoInfo = await helpers.geoIP(ip);
            let city = geoInfo.city.replace(/ +/,'+');
            let countrycode = geoInfo.countrycode;
            return helpers.currentWeather(city,countrycode);
        }else{
            let city = req.params.city.replace(/ +/,'+');
            return helpers.currentWeather(city);
        }
    })

    fastify.get(env.apiversion + '/forecast/:city', async (req, res) => {

        if(req.params.city === ''){
            let ip = await helpers.publicIP();
            let geoInfo = await helpers.geoIP(ip);
            let city = geoInfo.city.replace(/ /,'+');
            let countrycode = geoInfo.countrycode;
            return helpers.forecastWeather(city,countrycode);
        }else{
            let city = req.params.city.replace(/ /,'+');
            return helpers.forecastWeather(city);
        }
    })
  }
  
  module.exports = routes