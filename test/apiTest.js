const tap = require('tap')
const supertest = require('supertest')
const startServer = require('../app')

const fastify = startServer();

tap.test('Testing Method GET for `/v1/location` endpoint', async (t) => {

        await fastify.ready()

        const response = await supertest(fastify.server)
            .get('/v1/location')
            .set('Accept', 'application/json')
            .expect(200)
            .expect('Content-Type', 'application/json; charset=utf-8')
            t.type(response.body, 'object','Response must be a JSON Object')
            t.match(response.body, {'city':/(.*?)/},'Response Object must contain "city" parameter',{diagnostic:false})
            t.match(response.body, {'country':/(.*?)/},'Response must contain "country" parameter',{diagnostic:false})
            t.end() 


        })
    
        tap.test('Testing Method GET for `/v1/current/`', async (t) => {
        
            await fastify.ready()
        
            const response = await supertest(fastify.server)
            .get('/v1/current/')
            .set('Accept', 'application/json')
            .expect(200)
            .expect('Content-Type', 'application/json; charset=utf-8')
            t.type(response.body, 'object','Response must be a JSON Object')
            t.match(response.body, {'name':/(.*?)/},'Response Object must contain "city" parameter',{diagnostic:false})
            t.match(response.body, {'cod':200},'Response must contain status 200',{diagnostic:false})
            t.end()
        })

     tap.test('Testing Method GET for `/v1/current/:city` endpoint with correct value for city', async (t) => {
        
            await fastify.ready()
        
            const response = await supertest(fastify.server)
            .get('/v1/current/buenos+aires')
            .set('Accept', 'application/json')
            .expect(200)
            .expect('Content-Type', 'application/json; charset=utf-8')
            t.type(response.body, 'object','Response must be a JSON Object')
            t.match(response.body, {'name':/(.*?)/},'Response Object must contain "city" parameter',{diagnostic:false})
            t.match(response.body, {'cod':200},'Response must contain status 200',{diagnostic:false})
            t.end() 

        })

     tap.test('Testing Method GET for `/v1/forecast/` endpoint', async (t) => {
        
            await fastify.ready()
        
            const response = await supertest(fastify.server)
            .get('/v1/forecast/')
            .set('Accept', 'application/json')
            .expect(200)
            .expect('Content-Type', 'application/json; charset=utf-8')
            t.type(response.body, 'object','Response must be a JSON Object')
            t.match(response.body, {'city':/(.*?)/},'Response Object must contain "city" parameter',{diagnostic:false})
            t.match(response.body, {'cod':200},'Response must contain status 200',{diagnostic:false})
            t.match(response.body, {'list':/(.*?)/},'Response Object must contain a list with forecast elements',{diagnostic:false})
            t.end() 

        })
        
     tap.test('Testing Method GET for `/v1/forecast/:city` endpoint with correct value for city', async (t) => {
        
            await fastify.ready()
        
            const response = await supertest(fastify.server)
            .get('/v1/forecast/buenos+aires')
            .set('Accept', 'application/json')
            .expect(200)
            .expect('Content-Type', 'application/json; charset=utf-8')
            t.type(response.body, 'object','Response must be a JSON Object')
            t.match(response.body, {'city':/(.*?)/},'Response Object must contain "city" parameter',{diagnostic:false})
            t.match(response.body, {'cod':200},'Response must contain status 200',{diagnostic:false})
            t.match(response.body, {'list':/(.*?)/},'Response Object must contain a list with forecast elements',{diagnostic:false})
            t.end() 

        })


     tap.test('Testing Method GET for `/v1/current/:city` endpoint with incorrect value for city', async (t) => {
        
            await fastify.ready()
        
            const response = await supertest(fastify.server)
            .get('/v1/current/a+aires')
            .set('Accept', 'application/json')
            .expect(200)
            .expect('Content-Type', 'application/json; charset=utf-8')
            t.type(response.body, 'object','Response must be a JSON Object')
            t.deepEqual(response.body, {'status':404},'Response must contain "Status" = 404',{diagnostic:false})
            t.end() 

        })


     tap.test('Testing Method GET for `/v1/forecast/:city` endpoint with incorrect value for city', async (t) => {
        
            await fastify.ready()
        
            const response = await supertest(fastify.server)
            .get('/v1/forecast/a+aires')
            .set('Accept', 'application/json')
            .expect(200)
            .expect('Content-Type', 'application/json; charset=utf-8')
            t.type(response.body, 'object','Response must be a JSON Object')
            t.deepEqual(response.body, {'status':404},'Response must contain "Status" = 404',{diagnostic:false})
            t.end();
        })

        tap.tearDown(()=>{process.exit(0)});    
