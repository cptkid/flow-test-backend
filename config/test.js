'use strict';

module.exports = {
  env: 'test',
  weatherapikey: '23e5d6d60949a3770f89390b4128bcbc',
  apiversion: '/v1',
  openWeatherURL: 'https://api.openweathermap.org/data/2.5/'
};